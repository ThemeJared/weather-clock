import React from 'react';
import Background from './components/Background.js';
import WeatherCurrent from './components/WeatherCurrent.js';
import Time from './components/Time.js';
import ForcastDay from './components/ForcastDay.js';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            test: 'foo'
        };
    }
    componentDidMount() {
        setInterval(() => {
            var time = new Date();
            var hours = time.getHours();
            const element = {
                dimmer: document.getElementById('dimmer')
            }
            const intensity = {
                none: 0,
                low: 0.25,
                medium: 0.50,
                high: 0.75
            }
            var currentIntensity = undefined;

            switch (hours) {
                case 18:
                    currentIntensity = (intensity.low);
                    break;
                case 19:
                    currentIntensity = (intensity.medium);
                    break;
                case 20:
                    currentIntensity = (intensity.high);
                    break;
                case 21:
                    currentIntensity = (intensity.high);
                    break;
                case 22:
                    currentIntensity = (intensity.high);
                    break;
                case 23:
                    currentIntensity = (intensity.high);
                    break;
                case 24:
                    currentIntensity = (intensity.high);
                    break;
                case 1:
                    currentIntensity = (intensity.high);
                    break;
                case 2:
                    currentIntensity = (intensity.high);
                    break;
                case 3:
                    currentIntensity = (intensity.high);
                    break;
                case 4:
                    currentIntensity = (intensity.high);
                    break;
                case 5:
                    currentIntensity = (intensity.medium);
                    break;
                case 6:
                    currentIntensity = (intensity.low);
                    break;
                default:
                    currentIntensity = (intensity.none);
            }

            element.dimmer.style.opacity = currentIntensity;
        }, 600000);
        var time = new Date();
        var hours = time.getHours();
        const element = {
            dimmer: document.getElementById('dimmer')
        }
        const intensity = {
            none: 0,
            low: 0.25,
            medium: 0.50,
            high: 0.75
        }
        var currentIntensity = undefined;

        switch (hours) {
            case 18:
                currentIntensity = (intensity.low);
                break;
            case 19:
                currentIntensity = (intensity.medium);
                break;
            case 20:
                currentIntensity = (intensity.high);
                break;
            case 21:
                currentIntensity = (intensity.high);
                break;
            case 22:
                currentIntensity = (intensity.high);
                break;
            case 23:
                currentIntensity = (intensity.high);
                break;
            case 24:
                currentIntensity = (intensity.high);
                break;
            case 1:
                currentIntensity = (intensity.high);
                break;
            case 2:
                currentIntensity = (intensity.high);
                break;
            case 3:
                currentIntensity = (intensity.high);
                break;
            case 4:
                currentIntensity = (intensity.high);
                break;
            case 5:
                currentIntensity = (intensity.medium);
                break;
            case 6:
                currentIntensity = (intensity.low);
                break;
            default:
                currentIntensity = (intensity.none);
        }

        element.dimmer.style.opacity = currentIntensity;
    }
    render() {
        return (
            <div>
                <div id='dimmer'></div>
                <div className="row" style={{
                    'marginTop': '10px'
                }}>
                    <div className="col-xs-8"><WeatherCurrent/></div>
                    <div className="col-xs-4"><Time/></div>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <ForcastDay day={1}/>
                        <ForcastDay day={2}/>
                        <ForcastDay day={3}/>
                    </div>
                    <div className="col-xs-4"></div>
                </div>
                <Background/>
            </div>
        );
    }
}
