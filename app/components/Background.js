import React, {Component} from 'react';

export default class Background extends Component {

	render() {
		const theme = 'gray';

		return(
			<div className='svg-background-container'>
				<div id='svg-background-container-overlay' style={{'zIndex': '-2'}}></div>
				<div id='svg-background-container-layer-1' className='svg-background-container-layer' style={{'backgroundImage': 'url("media/background_layers/' + theme + '/background_layer_1.svg")', 'zIndex': '-3'}}></div>
				<div id='svg-background-container-layer-2' className='svg-background-container-layer' style={{'backgroundImage': 'url("media/background_layers/' + theme + '/background_layer_2.svg")', 'zIndex': '-4'}}></div>
				<div id='svg-background-container-layer-3' className='svg-background-container-layer' style={{'backgroundImage': 'url("media/background_layers/' + theme + '/background_layer_3.svg")', 'zIndex': '-5'}}></div>
			</div>
		)
	}
}
