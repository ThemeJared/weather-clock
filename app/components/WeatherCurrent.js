import React, {Component} from 'react';
import weatherDataSchema from '../stores/weather_data_schema.json';

export default class WeatherCurrent extends Component {
	constructor(props) {
		super(props);
		this.state = weatherDataSchema;
	}

	componentDidMount() {
		socket.emit('request-weather-data');
		socket.on('return-weather-data', function(weatherData){
			this.setState(weatherData);
		}.bind(this));

		setInterval(() => {
			socket.emit('request-weather-data');
		}, 2100000);

	}

	render() {
		const currentObservationIconUrl = ('http://icons.wxug.com/i/c/v4/' + (this.state.current_observation.icon_url.replace('http://icons.wxug.com/i/c/k/', '').replace('.gif', '')) + '.svg');

		return(
			<div className='weather-container'>
				<img className='weather-container-icon' src={currentObservationIconUrl}></img>
				<div style={{'display': 'inline-table'}}>
					<span className='weather-container-temp'>{this.state.current_observation.temp_f}°</span>
					<span className='weather-container-temp-subtext'>
						<span style={{'color':'#EF9A9A'}}> {this.state.forecast.simpleforecast.forecastday[0].high.fahrenheit}° </span>
						 |
						<span style={{'color':'#90CAF9'}}> {this.state.forecast.simpleforecast.forecastday[0].low.fahrenheit}° </span>
						<img style={{'height':'21px', 'position':'relative', 'top':'-3px', 'paddingLeft':'15px'}} src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDg0Ny4zNzIgODQ3LjM3MiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgODQ3LjM3MiA4NDcuMzcyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTQwNi4yNjksMTAuMDUybC0yMzIuNjUsNDA1Ljc0MWMtNDguODg5LDg1Ljc3OS01Mi42NjUsMTk0Ljg1LDAsMjg2LjY5N2M3OS4xNjksMTM4LjA3LDI1NS4yNzcsMTg1LjgyLDM5My4zNDgsMTA2LjY1ICAgYzEzOC4wNzEtNzkuMTY5LDE4NS44MjEtMjU1LjI3NiwxMDYuNjUxLTM5My4zNDhMNDQwLjk2OCwxMC4wNTJDNDMzLjI4My0zLjM1MSw0MTMuOTUzLTMuMzUxLDQwNi4yNjksMTAuMDUyeiIgZmlsbD0iIzkwY2FmOSIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
						<span> {this.state.forecast.simpleforecast.forecastday[0].pop}% </span>
					</span>
				</div>
			</div>
		)
	}
}
