import React, {Component} from 'react';

export default class Time extends Component {
    constructor(props) {
        super(props);
        let date = new Date;
        date.setTime(Date.now())
        let time = date.getHours();
        let hours12;
        let hoursAmPm;
		let dayOfWeek;
		switch (new Date().getDay()) {
		    case 0:
		        dayOfWeek = "Sunday";
		        break;
		    case 1:
		        dayOfWeek = "Monday";
		        break;
		    case 2:
		        dayOfWeek = "Tuesday";
		        break;
		    case 3:
		        dayOfWeek = "Wednesday";
		        break;
		    case 4:
		        dayOfWeek = "Thursday";
		        break;
		    case 5:
		        dayOfWeek = "Friday";
		        break;
		    case 6:
		        dayOfWeek = "Saturday";
		}
        if (date.getHours() <= 12) {
            //AM
            hours12 = date.getHours();
            hoursAmPm = 'AM';
        } else {
            //PM
            hours12 = (date.getHours() - 12);
            hoursAmPm = 'PM';
        }

        this.state = {
            time: {
				month: date.getMonth(),
                dayOfWeek: dayOfWeek,
                day: date.getDate(),
                hours24: date.getHours(),
                hours12: hours12,
                hoursAmPm: hoursAmPm,
                minutes: date.getMinutes(),
                seconds: date.getSeconds()
            }
        }
    }

    componentDidMount() {
        setInterval(() => {
            let date = new Date;
            date.setTime(Date.now())
            let time = date.getHours();
            let hours12;
            let hoursAmPm;
			let dayOfWeek;
			switch (new Date().getDay()) {
			    case 0:
			        dayOfWeek = "Sunday";
			        break;
			    case 1:
			        dayOfWeek = "Monday";
			        break;
			    case 2:
			        dayOfWeek = "Tuesday";
			        break;
			    case 3:
			        dayOfWeek = "Wednesday";
			        break;
			    case 4:
			        dayOfWeek = "Thursday";
			        break;
			    case 5:
			        dayOfWeek = "Friday";
			        break;
			    case 6:
			        dayOfWeek = "Saturday";
			}
            if (date.getHours() <= 12) {
                //AM
                hours12 = date.getHours();
                hoursAmPm = 'AM';
            } else {
                //PM
                hours12 = (date.getHours() - 12);
                hoursAmPm = 'PM';
            }
            this.setState({
                time: {
					month: date.getMonth(),
                    dayOfWeek: dayOfWeek,
                    day: date.getDate(),
                    hours24: date.getHours(),
                    hours12: hours12,
                    hoursAmPm: hoursAmPm,
                    minutes: date.getMinutes(),
                    seconds: date.getSeconds()
                }
            })
        }, 100);
    }

    render() {
        return (
            <div>
                <h1 className='time-display'>{this.state.time.hours12}:{this.state.time.minutes < 10 ? '0':''}{this.state.time.minutes}
                    <h1 className='time-date'>
                        {this.state.time.dayOfWeek} <span style={{'fontWeight':'300'}}>{(this.state.time.month + 1)}/{this.state.time.day}</span>
                    </h1>
                </h1>
            </div>
        )
    }
}
