import React, {Component} from 'react';
import weatherDataSchema from '../stores/weather_data_schema.json';

export default class ForcastDay extends Component {
	constructor(props) {
		super(props);
		this.state = weatherDataSchema;
		this.id = Math.floor(Math.random()*9999999999999999);
	}

	componentDidMount() {
		socket.on('return-weather-data', function(weatherData){
			this.setState(weatherData);
		}.bind(this));
		const precipitationWidth = document.getElementById(this.id).offsetWidth;
		const precipitationMargin = (((150 / 2) - (precipitationWidth / 2)) + 'px');
		document.getElementById(this.id).style.marginLeft = precipitationMargin;

	}

	render() {
		const currentObservationIconUrl = ('http://icons.wxug.com/i/c/v4/' + (this.state.forecast.simpleforecast.forecastday[this.props.day].icon_url.replace('http://icons.wxug.com/i/c/k/', '').replace('.gif', '')) + '.svg');
		const weekday = this.state.forecast.simpleforecast.forecastday[this.props.day].date.weekday.substring(0, 3);

		setTimeout(() => {
			const precipitationWidth = document.getElementById(this.id).offsetWidth;
			const precipitationMargin = (((150 / 2) - (precipitationWidth / 2)) + 'px');
			document.getElementById(this.id).style.marginLeft = precipitationMargin;
		}, 200);

		return(
			<div className='forecast-container'>
				<h3 className='forecast-container-date'><span style={{'fontWeight':'500'}}>{weekday}</span> {this.state.forecast.simpleforecast.forecastday[this.props.day].date.month}/{this.state.forecast.simpleforecast.forecastday[this.props.day].date.day}</h3>
				<img className='forecast-container-icon' src={currentObservationIconUrl}></img>
				<h3 className='forecast-container-date' style={{'fontWeight':'700', 'marginBottom': '5px'}}>
					<span style={{'color':'#EF9A9A'}}> {this.state.forecast.simpleforecast.forecastday[this.props.day].high.fahrenheit}° </span>
					 |
					<span style={{'color':'#90CAF9'}}> {this.state.forecast.simpleforecast.forecastday[this.props.day].low.fahrenheit}° </span>
				</h3>
				<div id={this.id} className='forecast-container-precipitation'>
					<img style={{'height':'18px', 'position':'relative', 'top':'-3px'}} src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDg0Ny4zNzIgODQ3LjM3MiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgODQ3LjM3MiA4NDcuMzcyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBhdGggZD0iTTQwNi4yNjksMTAuMDUybC0yMzIuNjUsNDA1Ljc0MWMtNDguODg5LDg1Ljc3OS01Mi42NjUsMTk0Ljg1LDAsMjg2LjY5N2M3OS4xNjksMTM4LjA3LDI1NS4yNzcsMTg1LjgyLDM5My4zNDgsMTA2LjY1ICAgYzEzOC4wNzEtNzkuMTY5LDE4NS44MjEtMjU1LjI3NiwxMDYuNjUxLTM5My4zNDhMNDQwLjk2OCwxMC4wNTJDNDMzLjI4My0zLjM1MSw0MTMuOTUzLTMuMzUxLDQwNi4yNjksMTAuMDUyeiIgZmlsbD0iIzkwY2FmOSIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" /> <h3 style={{'display': '-webkit-inline-box'}} className='forecast-container-date'>{this.state.forecast.simpleforecast.forecastday[this.props.day].snow_allday.in}" </h3> <img style={{'height':'18px', 'position':'relative', 'top':'-3px', 'paddingLeft':'15px'}} src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTYuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjMycHgiIGhlaWdodD0iMzJweCIgdmlld0JveD0iMCAwIDQ1Ny4yOTMgNDU3LjI5MyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDU3LjI5MyA0NTcuMjkzOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxnPgoJPHBvbHlnb24gcG9pbnRzPSIzMjYuOTg0LDIyMS41NTUgNDM0LjU5NCwyNzYuMzg2IDQ1Ny4yOTMsMjMxLjgzNSAzOTQuMjMyLDE5OS43MDUgNDMyLjAwNiwxODcuNDMyIDQxNi41NTUsMTM5Ljg3OCAgICAzNzguNzgzLDE1Mi4xNTEgNDEwLjkxNCw4OS4wODkgMzY2LjM2Myw2Ni4zOTEgMzExLjUzMywxNzQuMDAxIDI1My42NDYsMTkyLjgxMSAyNTMuNjQ2LDEzMS45NDUgMzM5LjA0Nyw0Ni41NDUgMzAzLjY5MSwxMS4xOSAgICAyNTMuNjQ2LDYxLjIzNCAyNTMuNjQ2LDIxLjUxOSAyMDMuNjQ2LDIxLjUxOSAyMDMuNjQ2LDYxLjIzNCAxNTMuNjAxLDExLjE5IDExOC4yNDYsNDYuNTQ1IDIwMy42NDYsMTMxLjk0NSAyMDMuNjQ2LDE5Mi44MTEgICAgMTQ1Ljc2MSwxNzQuMDAxIDkwLjkzMSw2Ni4zOTIgNDYuMzgsODkuMDkgNzguNTExLDE1Mi4xNSA0MC43MzgsMTM5Ljg3NyAyNS4yODcsMTg3LjQzMSA2My4wNiwxOTkuNzA1IDAsMjMxLjgzNSAyMi42OTksMjc2LjM4NiAgICAxMzAuMzEsMjIxLjU1NSAxODguMTk1LDI0MC4zNjIgMTUyLjQxOSwyODkuNjA0IDMzLjEzMiwzMDguNDk3IDQwLjk1NSwzNTcuODggMTEwLjg1NywzNDYuODExIDg3LjUxMywzNzguOTQgMTI3Ljk2NCw0MDguMzMgICAgMTUxLjMwOCwzNzYuMTk5IDE2Mi4zOCw0NDYuMTAzIDIxMS43NjUsNDM4LjI4IDE5Mi44NzEsMzE4Ljk5MyAyMjguNjQ2LDI2OS43NTMgMjY0LjQyMSwzMTguOTkzIDI0NS41MjksNDM4LjI3OSAyOTQuOTE0LDQ0Ni4xMDIgICAgMzA1Ljk4NCwzNzYuMTk5IDMyOS4zMyw0MDguMzMgMzY5Ljc4MSwzNzguOTQgMzQ2LjQzNiwzNDYuODExIDQxNi4zNCwzNTcuODgyIDQyNC4xNjIsMzA4LjQ5NyAzMDQuODczLDI4OS42MDQgMjY5LjA5OCwyNDAuMzYyICAgICAiIGZpbGw9IiM5MGNhZjkiLz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K" /> <h3 style={{'display': '-webkit-inline-box'}} className='forecast-container-date'>{this.state.forecast.simpleforecast.forecastday[this.props.day].snow_allday.in}"</h3>
				</div>
			</div>
		)
	}
}
