var io = require('./socketIo.js');
var Wunderground = require('wundergroundnode');
var fs = require('fs');
// Get an API key
// https://www.wunderground.com/weather/api/
var key = ['ENTER YOUR API KEY HERE', 'ENTER YOUR API KEY HERE', 'ENTER YOUR API KEY HERE'];
var keyCycle = 0;
var zipcode = 02048;
var wunderground = new Wunderground(key[keyCycle]);

io.on('connection', function(socket){
    socket.on('request-weather-data', function () {
		if (keyCycle === 0) {
			keyCycle = 1;
		} else if (keyCycle === 1) {
            keyCycle = 2
        } else if (keyCycle === 2) {
            keyCycle = 0
        }


            wunderground.conditions().hourlyForecast().forecast().astronomy().request(zipcode, function(err, response){
                socket.emit('return-weather-data', response);
                fs.writeFile('./app/stores/weather_data_schema.json', JSON.stringify(response), 'utf8', (err) => {
                    if (err) throw err;
                });
            });

    });
});
