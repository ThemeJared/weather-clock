var app = require('express')();
var http = require('http').Server(app);
http.listen(3000, function(){
  console.log('socket-io on port 3000');
});

module.exports = require('socket.io')(http);
